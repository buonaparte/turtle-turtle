<?php

namespace Turtle\Component\Pager;

use Traversable;

class PageRange implements
    \Countable,
    \IteratorAggregate
{
    protected $options = array(
        'pageCount' => 0,
        'perPage' => 10,
        'first' => 1,
        'last' => 1,
        'current' => 1,
        'previous' => null,
        'next' => null,

        'pagesInRange' => array(),
        'firstPageInRange' => null,
        'lastPageInRange' => null,

        'currentItemCount' => 0,
        'totalItemCount' => 0,
        'firstItem' => 0,
        'lastItem' => 0
    );

    public function __construct(array $options = array())
    {
        $this->options = array_replace($this->options, $options);
    }

    public function getPageCount()
    {
        return $this->options['pageCount'];
    }

    public function getItemsPerPage()
    {
        return $this->options['perPage'];
    }

    public function getFirst()
    {
        return $this->options['first'];
    }

    public function getLast()
    {
        return $this->options['last'];
    }

    public function getCurrentPage()
    {
        return $this->options['current'];
    }

    public function isCurrentPage($page)
    {
        return $this->getCurrentPage() == $page;
    }

    public function hasPrevious()
    {
        return $this->options['previous'] != $this->options['current'];
    }

    public function hasNext()
    {
        return $this->options['next'] != $this->options['current'];
    }

    public function getPrevious()
    {
        return $this->hasPrevious() ? $this->options['previous'] : null;
    }

    public function getNext()
    {
        return $this->hasNext() ? $this->options['next'] : null;
    }

    public function getPagesInRange()
    {
        return $this->options['pagesInRange'];
    }

    public function getFirstPageInRange()
    {
        if (! isset($this->options['firstPageInRange'])) {
            $this->options['firstPageInRange'] = min($this->getCurrentPage());
        }

        return $this->options['firstPageInRange'];
    }

    public function getLastPageInRange()
    {
        if (! isset($this->options['lastPageInRange'])) {
            $this->options['lastPageInRange'] = max($this->options['pagesInRange']);
        }

        return $this->options['lastPageInRange'];
    }

    public function getCurrentPageItemCount()
    {
        return $this->options['currentItemCount'];
    }

    public function getTotalItemCount()
    {
        return $this->options['totalItemCount'];
    }

    public function getFirstItemNumber()
    {
        return $this->options['firstItem'];
    }

    public function getLastItemNumber()
    {
        return $this->options['lastItem'];
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->options['pagesInRange']);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     */
    public function count()
    {
        return count($this->options['pagesInRange']);
    }
}