<?php

namespace Turtle\Component\Paginator\Adapter\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}