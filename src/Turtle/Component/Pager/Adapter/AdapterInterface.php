<?php

namespace Turtle\Component\Pager\Adapter;

interface AdapterInterface extends \Countable
{
    public function getItems($offset, $itemsPerPage);
}