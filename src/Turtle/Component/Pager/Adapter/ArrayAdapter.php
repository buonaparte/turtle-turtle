<?php

namespace Turtle\Component\Pager\Adapter;

use Turtle\Stl\ArrayUtils;

class ArrayAdapter implements AdapterInterface
{
    protected $items;
    protected $count;

    public function __construct($items)
    {
        $this->items = ArrayUtils::toArray($items);
        $this->count = count($this->items);
    }

    public function getItems($offset, $itemsPerPage)
    {
        return array_slice($this->items, $offset, $itemsPerPage);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     */
    public function count()
    {
        return $this->count;
    }
}