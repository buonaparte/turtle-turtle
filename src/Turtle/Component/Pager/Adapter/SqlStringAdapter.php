<?php

namespace Turtle\Component\Pager\Adapter;

use Turtle\Component\Paginator\Adapter\Exception\InvalidArgumentException;
use Turtle\Stl\ArrayUtils;
use Turtle\Stl\Hydrator\HydratorInterface;

class SqlStringAdapter implements AdapterInterface
{
    const OPTIONS_KEY_HYDRATOR = 'hydrator';
    const OPTIONS_KEY_RESULT_ITEM_PROTOTYPE = 'resultItemPrototype';
    const OPTION_KEY_LIMIT_OFFSET_TRANSLATOR = 'limitOffsetTranslator';

    /**
     * Raw Sql string
     *
     * @var string
     */
    protected $sql;

    /**
     * Execute statement callback
     * Will only return a Collection or Iterator of Dictionaries
     *
     * @var callable
     */
    protected $statementCallback;

    /**
     * @var int
     */
    protected $count;

    /**
     * @var int|null
     */
    protected $limit;

    /**
     * @var int|null
     */
    protected $offset;

    /**
     * @var HydratorInterface
     */
    protected $hydrator;

    /**
     * @var object
     */
    protected $resultItemPrototype;

    public function __construct($sql, $callback)
    {
        if (! is_callable($callback)) {
            throw new InvalidArgumentException(sprintf(
                '%s requires a valid execute statement callback, %s was provided',
                get_class($this),
                gettype($callback)
            ));
        }

        $this->sql = $sql;
        $this->statementCallback = $callback;
    }

    public function getItems($offset, $itemsPerPage)
    {
        // ensure we prepared everything
        $this->getCount();

        $sql = preg_replace(
            '#limit\\s[0-9]+(\\s*,\\s*[0-9]+)#i',
            '',
            preg_replace('#offset\\s[0-9]+#i', '', $this->sql)
        );

        $sql = $this->appendLimitClause($sql, $offset, $itemsPerPage);

        $result = call_user_func($this->statementCallback, $sql);
        if (empty($result)) {
            return array();
        }

        $result = ArrayUtils::toArray($result);
        if (null === $this->hydrator) {
            return $result;
        }

        if (! is_object($this->resultItemPrototype)) {
            throw new InvalidArgumentException(
                'A valid object must be specified as a Result Item Prototype, when used with a Hydrator');
        }

        $hydrator = $this->hydrator;
        $itemPrototype = $this->resultItemPrototype;

        return array_map(function ($item) use ($hydrator, $itemPrototype) {
            return $hydrator->hydrate($item, clone $itemPrototype);
        }, $result);
    }

    protected function appendLimitClause($sql, $offset, $itemsPerPage)
    {
        if (null !== $this->limit) {
            $itemsPerPage = $this->limit + $itemsPerPage;
        }

        if (null !== $this->offset) {
            $offset = $this->offset + $offset;
        }

        return "$sql limit $itemsPerPage offset $offset";
    }

    protected function getCount()
    {
        if (null === $this->count) {
            $this->count = 0;

            $matches = array();
            if (preg_match('#limit\\s+([0-9]+)(\\s*,\\s*[0-9]+)?#i', $this->sql, $matches)) {
                $this->limit = (int) $matches[1];
                if (! empty($matches[2])) {
                    $this->offset = (int) ltrim($matches[2], ' ,');
                }
            }

            if (null === $this->offset) {
                $matches = array();
                if (preg_match('#offset ([0-9]+)#', $this->sql, $matches) && ! empty($matches[1])) {
                    $this->offset = (int) $matches;
                }
            }

            foreach (array('limit', 'offset') as $target) {
                if (null !== $this->{$target} && 0 >= $this->{$target}) {
                    $this->{$target} = null;
                }
            }

            // Count
            $sql = preg_replace('#select (.*) from#i', 'select COUNT(*) as __ic from', $this->sql);

            if ($result = call_user_func($this->statementCallback, $sql)) {
                $result = ArrayUtils::toArray($result);
                $this->count = ! empty($result[0]['__ic']) ? $result[0]['__ic'] : 0;
            }
        }

        return $this->count;
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     */
    public function count()
    {
        return $this->getCount();
    }

    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
        return $this;
    }

    public function setResultItemPrototype($itemPrototype)
    {
        $this->resultItemPrototype = $itemPrototype;
        return $this;
    }
}