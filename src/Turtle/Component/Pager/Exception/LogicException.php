<?php

namespace Turtle\Component\Pager\Exception;

class LogicException extends \LogicException implements ExceptionInterface
{
}