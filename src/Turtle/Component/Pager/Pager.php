<?php

namespace Turtle\Component\Pager;

use Traversable;
use Turtle\Component\Pager\Adapter\AdapterInterface;
use Turtle\Component\Pager\Exception\LogicException;
use Turtle\Component\Pager\RangeStyle\RangeStyleInterface;
use Turtle\Component\Pager\RangeStyle\Sliding;
use Turtle\Stl\ArrayUtils;

class Pager implements PagerInterface
{
    /**
     * @var AdapterInterface
     */
    protected $adapter;

    /**
     * @var int
     */
    protected $currentPageItemCount;

    /**
     * @var array|\Traversable
     */
    protected $currentPageItems;

    /**
     * @var int
     */
    protected $currentPage = 1;

    /**
     * @var int
     */
    protected $itemsPerPage = 10;

    /**
     * Page Range
     *
     * @var int
     */
    protected $pageRange = 10;

    /**
     * @var int
     */
    protected $pageCount;

    /**
     * @var RangeStyleInterface
     */
    protected $rangeStyle;

    /**
     * @var array|\Traversable
     */
    protected $pages;

    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    public function setPageRange($pageRange)
    {
        $this->pageRange = (int) $pageRange;
        return $this;
    }

    public function getPageRange()
    {
        return $this->pageRange;
    }

    public function setRangeStyle(RangeStyleInterface $rangeStyle)
    {
        $this->rangeStyle = $rangeStyle;
        return $this;
    }

    public function getRangeStyle()
    {
        if (null === $this->rangeStyle) {
            $this->setRangeStyle(new Sliding());
        }

        return $this->rangeStyle;
    }

    public function getPagesBetween($head, $tail)
    {
        $head = $this->normalizePage($head);
        $tail = $this->normalizePage($tail);

        $pages = array();

        for ($i = $head; $i <= $tail; $i += 1) {
            $pages[$i] = $i;
        }

        return $pages;
    }

    public function getCurrentPageItems()
    {
        if (null === $this->currentPageItems) {
            $this->currentPageItems = $this->getItems();
        }

        return $this->currentPageItems;
    }

    /**
     * Return a PageRange instance based on the provided
     * RangeStyle\RangeStyleInterface
     *
     * @param RangeStyleInterface $rangeStyle|null
     * @return PageRange
     */
    public function getPages(RangeStyleInterface $rangeStyle = null)
    {
        if (null === $rangeStyle) {
            $rangeStyle = $this->getRangeStyle();
        }

        $style = get_class($rangeStyle);

        if (null === $this->pages || null === $this->pages[$style]) {
            $this->pages[$style] = $this->createPages($rangeStyle);
        }

        return $this->pages[$style];
    }

    protected function createPages(RangeStyleInterface $rangeStyle)
    {
        $count = count($this);
        $current = $this->getCurrentPage();

        $pages = new \stdClass();
        $pages->pageCount = $count;
        $pages->perPage = $this->getItemsPerPage();
        $pages->first = 1;
        $pages->current = $current;
        $pages->last = $count;

        if (0 < $prev = $current - 1) {
            $pages->previous = $prev;
        }

        if ($count >= $next = $current + 1) {
            $pages->next = $next;
        }

        $pages->pagesInRange = $rangeStyle->getPages($this);
        $pages->firstPageInRange = min($pages->pagesInRange);
        $pages->lastPageInRange = max($pages->pagesInRange);

        if (null !== $this->getItems()) {
            $pages->currentItemCount = $this->getCurrentItemCount();
            $pages->totalItemCount = $this->getTotalItemCount();
            $pages->firstItem = ($current - 1) * $this->getItemsPerPage() + 1;
            $pages->lastItem = $pages->firstItem + $pages->currentItemCount - 1;
        }

        return new PageRange(get_object_vars($pages));
    }

    /**
     * Set the maximum number of items per page
     *
     * @param $itemsPerPage
     * @return Pager current Pager instance for a fluent interface
     */
    public function setItemsPerPage($itemsPerPage = -1)
    {
        if (1 > $itemsPerPage) {
            $this->itemsPerPage = $this->getTotalItemCount();
        } else {
            $this->itemsPerPage = $itemsPerPage;
        }

        $this->currentPageItems = null;
        $this->currentPageItemCount = null;
        $this->pageCount = $this->computePageCount();

        return $this;
    }

    /**
     * Return the total number of items
     *
     * @return int
     */
    public function getTotalItemCount()
    {
        return count($this->adapter);
    }

    protected function computePageCount()
    {
        return ceil(count($this->getAdapter()) / $this->getItemsPerPage());
    }

    /**
     * Return the maximum number of items per page
     *
     * @return int
     */
    public function getItemsPerPage()
    {
        return $this->itemsPerPage;
    }

    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     */
    public function getIterator()
    {
        return $this->getItems();
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     */
    public function count()
    {
        if (null === $this->pageCount) {
            $this->pageCount = $this->computePageCount();
        }

        return $this->pageCount;
    }

    /**
     * Set the current page number
     *
     * @param $page
     * @return Pager current instance for fluent interface
     */
    public function setCurrentPage($page)
    {
        $this->currentPage = $this->normalizePage($page);
        $this->currentPageItems = null;
        $this->currentPageItemCount = null;

        return $this;
    }

    /**
     * Return the current page number
     *
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    public function getAbsoluteItemNumber($relative, $page = null)
    {
        $relative = $this->normalizeItem($relative);

        if (null === $page) {
            $page = $this->getCurrentPage();
        }
        $page = $this->normalizePage($page);

        return ($page - 1) * $this->getItemsPerPage() + $relative;
    }

    /**
     * Returns the number of items in current page
     *
     * @return int
     */
    public function getCurrentItemCount()
    {
        if (null === $this->currentPageItemCount) {
            $this->currentPageItemCount = $this->getItemCount($this->getCurrentPageItems());
        }

        return $this->currentPageItemCount;
    }

    public function getItemCount($items)
    {
        $count = 0;

        if (is_array($items) || $items instanceof \Countable) {
            $count = count($items);
        } elseif ($items instanceof Traversable) {
            $count = iterator_count($items);
        }

        return $count;
    }

    public function getItem($item, $page = null)
    {
        if (null === $page) {
            $page = $this->getCurrentPage();
        } else {
            $page = $this->normalizePage($page);
        }

        $items = $this->getItems($page);
        $count = $this->getItemCount($items);

        if (0 === $count) {
            throw new LogicException(sprintf('Page %s does not exist', $page));
        }

        if (0 > $item) {
            $item = ($count + 1) * $item;
        }

        $item = $this->normalizeItem($item);

        if ($item > $count) {
            throw new LogicException(sprintf('Page %s does not have item %s', $page, $item));
        }

        return $items[$item - 1];
    }

    public function getItems($page = null)
    {
        if (null === $page) {
            $page = $this->getCurrentPage();
        } else {
            $page = $this->normalizePage($page);
        }

        $offset = ($page - 1) * $this->getItemsPerPage();

        $items = $this->getAdapter()->getItems($offset, $this->getItemsPerPage());

        if ($items instanceof \Iterator) {
            return $items;
        }

        if ($items instanceof Traversable) {
            $items = ArrayUtils::toArray($items);
        }

        if (! is_array($items)) {
            $items = array($items);
        }

        return new \ArrayIterator($items);
    }

    /**
     * Return true if it's necessary to paginate or false if not
     *
     * @return bool
     */
    public function haveToPaginate()
    {
        return 1 < count($this);
    }

    public function normalizePage($page)
    {
        $page = (int) $page;

        if (1 > $page) {
            $page = 1;
        }

        $count = count($this);
        if (0 < $page && $page > $count) {
            $page = $count;
        }

        return $page;
    }

    public function normalizeItem($item)
    {
        $item = (int) $item;

        if (1 > $item) {
            $item = 1;
        }

        if ($item > $this->getItemsPerPage()) {
            $item = $this->getItemsPerPage();
        }

        return $item;
    }
}