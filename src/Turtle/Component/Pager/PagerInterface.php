<?php

namespace Turtle\Component\Pager;

use Turtle\Component\Pager\RangeStyle\RangeStyleInterface;

interface PagerInterface extends \Countable, \IteratorAggregate
{
    /**
     * Set the current page number
     *
     * @param $page
     */
    public function setCurrentPage($page);

    /**
     * Return true if it's necessary to paginate or false if not
     *
     * @return bool
     */
    public function haveToPaginate();

    /**
     * Return the total number of items
     *
     * @return int
     */
    public function getTotalItemCount();

    /**
     * Set the maximum number of items per page
     *
     * @param $itemsPerPage
     */
    public function setItemsPerPage($itemsPerPage = -1);

    /**
     * Return the maximum number of items per page
     *
     * @return int
     */
    public function getItemsPerPage();

    /**
     * Return the current page number
     *
     * @return int
     */
    public function getCurrentPage();


    /**
     * Returns the number of items in current page
     *
     * @return int
     */
    public function getCurrentItemCount();

    /**
     * Return a PageRange instance based on the provided
     * RangeStyle\RangeStyleInterface
     *
     * @param RangeStyleInterface $rangeStyle|null
     * @return PageRange
     */
    public function getPages(RangeStyleInterface $rangeStyle = null);
}