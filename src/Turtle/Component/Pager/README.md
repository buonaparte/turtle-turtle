Pager Component from Turtle Framework
=====================================

Pager allows you to paginate over arbitrary collections of data, using a simple
and intuitive API.

## Installation

The recommended way to install Turtle is [through
composer](http://getcomposer.org). Just create a `composer.json` file and
run the `php composer.phar install` command to install it:

    {
        "require": {
            "turtle/turtle": "dev-master"
        },
        "repositories": [
            {
                "type": "git",
                "url": "https://bitbucket.org/buonaparte/turtle-turtle.git"
            }
        ]
    }

## Usage

In order to paginate items into pages, Pager must have a generic way to access the data,
this is where the well - known Adapter design patterns comes into play. The Pager adapter must
implement the generic interface Adapter\AdapterInterface with 2 methods:

* `getItems($offset, $itemsPerPage)` - will provide a collection of max $itemPerPage items with an offset of $offset

* `count()` - inherited from Spl \Countable that is responsible to return a total of items aggregated

Several adapters ship with Pager Component by default:

* `Adapter\ArrayAdapter` - Aggregates PHP Array

* `Adapter\SqlStringAdapter` - Aggregates a Database raw SELECT query (fetching of items is done throw the
mandatory helper statementCallback)

To create an instance of Pager you must inject the Adapter as constructor argument:

```php
<?php
$pager = new \Turtle\Component\Pager\Pager(new \Turtle\Component\Pager\Adapter\ArrayAdapter($array));
// or using SqlString adapter:
$pdo = new \PDO();
$pager = new \Turtle\Component\Pager\Pager(new \Turtle\Component\Pager\Adapter\SqlStringAdapter(
    'SELECT * FROM `users` WHERE `active` = 1',
    function ($sqlQuery) use ($pdo) {
        $stmt = $pdo->query($sqlQuery);
        return $stmt ? $stmt->fetchAll(\PDO::FETCH_ASSOC) : array();
    }
));
```

The shipped Pager implements a PagerInterface with this minimal set of methods:

```php
<?php
// Set the current page number
$pager->setCurrentPage($page);

// Get the current page number
$pager->getCurrentPage();

// Return true if it's necessary to paginate or false if not
$pager->haveToPaginate();

// Return the total number of items
$pager->getTotalItemCount();

// Set the maximum number of items per page
// notice, a 0 or negative is the lazy equivalent of
// $pager->setItemsPerPage(count($pager->getAdapter()))
$pager->setItemsPerPage($itemsPerPage);

// Return the maximum number of items per page
$pager->getItemsPerPage();

// Returns the number of items in current page
$pager->getCurrentItemCount();

// Return a PageRange instance based on the provided
// RangeStyle\RangeStyleInterface
$pager->getPages(new \Turtle\Component\Pager\RangeStyle\Sliding());

// PagerInterface inherit getIterator() method from Spl \IteratorAggregate,
// thus making possible to iterate over current page items in a foreach loop:
foreach ($pager as $item) {
    // do stuff...
}

// and count() method from Spl \Countable
// which returns a total number of pages
count($pager);
```

Although many more helper methods available, check the source code.

# Range Style

There are some cases where simple pagination is not enough.
One example situation is when you want to write page links listings.
To enable a more powerful control over Pager, you can use RangeStyle package.
Currently, Turtle Pager implements two styles of ranges: Sliding and Jumping.

# Sliding

The page range will move smoothly with the current page.
The current page is always in the middle, except in the first and last pages of the range.
Check out how does it work with a chunk length of 5 items:

To set the chunk length, you can use $pager->setPageRange($chunkLength) method

    Listing 1 2 3 4 5 6 7 8 9 10 11 12 13 14
    Page 1: o-------|
    Page 2: |-o-----|
    Page 3: |---o---|
    Page 4:   |---o---|
    Page 5:     |---o---|
    Page 6:       |---o---|
    Page 7:         |---o---|
    Page 8:           |---o---|
    ...
    Page14:                       |-------o

# Jumping

In Jumping page range style, the range of page links is always one of a fixed set of “frames”:
1-5, 6-10, 11-15, and so on.

    Listing 1 2 3 4 5 6 7 8 9 10 11 12 13 14
    Page 1: o-------|
    Page 2: |-o-----|
    Page 3: |---o---|
    Page 4: |-----o-|
    Page 5: |-------o
    Page 6:           o---------|
    Page 7:           |-o-------|
    Page 8:           |---o-----|
    ...

To use one of the RangeStyles you just specify it as a default RangeStyle on the Pager,
or pass it as the optional argument to $pager->getPages($rangeStyle) method, ex:

```php
<?php
$pager = new \Turtle\Component\Pager\Pager(new Turtle\Component\Pager\Adapter\ArrayAdapter(
    range(0, 1000)
));

$pager->setItemsPerPage(5)
      ->setCurrentPage(2);

$pageRange = $pager->getPages(new Turtle\Component\Pager\RangeStyle\Sliding());
// and the Jumping style:
$pageRange = $pager->getPages(new Turtle\Component\Pager\RangeStyle\Jumping());
```

PageRange comes with a bunch of useful methods to help you render the pages section
on a web page, to inspect it, please refer to the Source Code.