<?php

namespace Turtle\Component\Pager\RangeStyle;

use Turtle\Component\Pager\Pager;

interface RangeStyleInterface
{
    public function getPages(Pager $pager, $range = null);
}