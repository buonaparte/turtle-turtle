<?php

namespace Turtle\Component\Pager\RangeStyle;

use Turtle\Component\Pager\Pager;

class Sliding implements RangeStyleInterface
{
    public function getPages(Pager $pager, $range = null)
    {
        if (null === $range) {
            $range = $pager->getPageRange();
        }

        $current = $pager->getCurrentPage();
        $count = count($pager);

        if ($range > $count) {
            $range = $count;
        }

        $delta = ceil($range / 2);

        if ($current - $delta > $count - $range) {
            $head = $count - $range + 1;
            $tail = $count;
        } else {
            if (0 > $current - $delta) {
                $delta = $current;
            }

            $offset = $current - $delta;
            $head = $offset + 1;
            $tail = $offset + $range;
        }

        return $pager->getPagesBetween($head, $tail);
    }
}