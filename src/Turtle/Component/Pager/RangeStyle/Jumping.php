<?php

namespace Turtle\Component\Pager\RangeStyle;

use Turtle\Component\Pager\Pager;

class Jumping implements RangeStyleInterface
{
    public function getPages(Pager $pager, $range = null)
    {
        if (null === $range) {
            $range = $pager->getPageRange();
        }

        $current = $pager->getCurrentPage();

        $delta = $current % $range;
        if (0 === $delta) {
            $delta = $range;
        }

        $offset = $current - $delta;
        $head = $offset + 1;
        $tail = $offset + $range;

        return $pager->getPagesBetween($head, $tail);
    }
}