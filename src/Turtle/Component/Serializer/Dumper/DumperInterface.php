<?php

namespace Turtle\Component\Serializer\Dumper;

interface DumperInterface
{
    /**
     * Dumps data instance into a string representation
     *
     * @param mixed $data
     * @return string
     */
    public function dump($data);
}