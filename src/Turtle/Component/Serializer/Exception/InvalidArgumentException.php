<?php

namespace Turtle\Component\Serializer\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}