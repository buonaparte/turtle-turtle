<?php

namespace Turtle\Component\Serializer\Exception;

class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}