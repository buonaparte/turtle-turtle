<?php

namespace Turtle\Component\Serializer;

use Turtle\Component\Serializer\Dumper\DumperInterface;
use Turtle\Component\Serializer\Exception\InvalidArgumentException;
use Turtle\Component\Serializer\Exception\RuntimeException;
use Turtle\Component\Serializer\Loader\LoaderInterface;
use Turtle\Stl\Hydrator\ClassMethods;

class Serializer
{
    const OPTIONS_KEY_USE_DEFAULT_LOADERS = 'useDefaultLoaders';
    const OPTIONS_KEY_USE_DEFAULT_DUMPERS = 'useDefaultDumpers';

    protected $options = array(
        self::OPTIONS_KEY_USE_DEFAULT_LOADERS => true,
        self::OPTIONS_KEY_USE_DEFAULT_DUMPERS => true
    );

    protected $loaders = array();
    protected $dumpers = array();

    public function __construct(array $options = array())
    {
        $this->options = array_replace($this->options, $options);
    }

    public function addLoader($format, $loader)
    {
        $this->loaders[$format] = $loader;
        return $this;
    }

    public function addDumper($format, $dumper)
    {
        $this->dumpers[$format] = $dumper;
        return $this;
    }

    public function loads($format)
    {
        if (! isset($this->loaders[$format])
            && $this->options[self::OPTIONS_KEY_USE_DEFAULT_LOADERS]
            && class_exists($loader = $this->computeDefaultLoaderName($format))
        ) {
            $this->addLoader($format, $loader);
        }

        return isset($this->loaders[$format]);
    }

    public function dumps($format)
    {
        if (! isset($this->dumpers[$format])
            && $this->options[self::OPTIONS_KEY_USE_DEFAULT_DUMPERS]
            && class_exists($dumper = $this->computeDefaultDumperName($format))
        ) {
            $this->addDumper($format, $dumper);
        }

        return isset($this->dumpers[$format]);
    }

    protected function computeDefaultLoaderName($format)
    {
        return 'Turtle\\Component\\Serializer\\Loader\\' . ucfirst($format);
    }

    protected function computeDefaultDumperName($format)
    {
        return 'Turtle\\Component\\Serializer\\Dumper\\' . ucfirst($format);
    }

    /**
     * @param $format
     * @return callable
     * @throws Exception\RuntimeException
     * @throws Exception\InvalidArgumentException
     */
    public function getLoader($format)
    {
        if (! $this->loads($format)) {
            throw new InvalidArgumentException(sprintf('No loader registered for %s format', $format));
        }

        $loader = $this->loaders[$format];
        if (! is_callable($loader) && is_string($loader) && class_exists($loader, true)) {
            $loader = new $loader();
        }

        if ($loader instanceof LoaderInterface) {
            $loader = array($loader, 'load');
        }

        if (! is_callable($loader)) {
            throw new RuntimeException(sprintf(
                'A Loader can only be a Class Name | LoaderInterface instance or callable, %s provided for %s format',
                gettype($loader),
                $format
            ));
        }

        $this->loaders[$format] = $loader;
        return $loader;
    }

    /**
     * @param $format
     * @return callable
     * @throws Exception\RuntimeException
     * @throws Exception\InvalidArgumentException
     */
    public function getDumper($format)
    {
        if (! $this->dumps($format)) {
            throw new InvalidArgumentException(sprintf('No dumper registered for %s format', $format));
        }

        $dumper = $this->dumpers[$format];
        if (! is_callable($dumper) && is_string($dumper) && class_exists($dumper, true)) {
            $dumper = new $dumper();
        }

        if ($dumper instanceof DumperInterface) {
            $dumper = array($dumper, 'load');
        }

        if (! is_callable($dumper)) {
            throw new RuntimeException(sprintf(
                'A Dumper can only be a Class Name | DumperInterface instance or callable, %s provided for %s format',
                gettype($dumper),
                $format
            ));
        }

        $this->dumpers[$format] = $dumper;
        return $dumper;
    }

    public function load($data, $format)
    {
        return call_user_func($this->getLoader($format), $data);
    }

    public function dump($data, $format)
    {
        return call_user_func($this->getDumper($format), $data);
    }

    public function serialize($data, $from, $to)
    {
        return call_user_func($this->getDumper($to), call_user_func($this->getLoader($from), $data));
    }

    public function unSerialize($data, $from, $to)
    {
        return call_user_func($this->getLoader($to), call_user_func($this->getDumper($from), $data));
    }
}