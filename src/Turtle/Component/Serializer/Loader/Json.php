<?php

namespace Turtle\Component\Serializer\Loader;

use Turtle\Component\Serializer\Loader\Exception\InvalidArgumentException;

class Json implements LoaderInterface
{
    protected $asAssoc;

    public function __construct($asAssoc = true)
    {
        $this->asAssoc($asAssoc);
    }

    public function asAssoc($asAssoc = null)
    {
        if (null === $asAssoc) {
            return $this->asAssoc;
        }

        $this->asAssoc = (boolean) $asAssoc;
        return $this;
    }

    /**
     * Attempts to load serialized data into a php understandable value
     *
     * @param mixed $data
     * @throws InvalidArgumentException
     * @return mixed
     */
    public function load($data)
    {
        if (! is_string($data) and ! is_object($data) || ! method_exists($data, '__toString')) {
            throw new InvalidArgumentException(
                'Can only load from a string, or an object implementing __toString');
        }

        return json_decode($data, $this->asAssoc);
    }
}