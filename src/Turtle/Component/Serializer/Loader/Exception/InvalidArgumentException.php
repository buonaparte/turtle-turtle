<?php

namespace Turtle\Component\Serializer\Loader\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}