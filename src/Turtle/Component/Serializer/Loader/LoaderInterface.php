<?php

namespace Turtle\Component\Serializer\Loader;

interface LoaderInterface
{
    /**
     * Attempts to load serialized data into a php understandable value
     *
     * @param mixed $data
     * @return mixed
     */
    public function load($data);
}