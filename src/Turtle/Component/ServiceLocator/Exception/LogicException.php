<?php
/**
 * Created by JetBrains PhpStorm.
 * User: buonaparte
 * Date: 4/30/13
 * Time: 4:30 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Turtle\Component\ServiceLocator\Exception;

class LogicException extends \LogicException implements ExceptionInterface
{
}