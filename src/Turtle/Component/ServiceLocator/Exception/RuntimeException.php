<?php
/**
 * Created by JetBrains PhpStorm.
 * User: buonaparte
 * Date: 4/29/13
 * Time: 8:07 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Turtle\Component\ServiceLocator\Exception;

class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}