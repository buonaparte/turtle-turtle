<?php
/**
 * Created by JetBrains PhpStorm.
 * User: buonaparte
 * Date: 4/29/13
 * Time: 3:56 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Turtle\Component\ServiceLocator\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}