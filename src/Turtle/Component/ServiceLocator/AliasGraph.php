<?php

namespace Turtle\Component\ServiceLocator;

use Turtle\Component\ServiceLocator\Exception\LogicException;

class AliasGraph
{
    protected $linksList = array();

    public function addAlias($name, $alias)
    {
        if ($name === $alias) {
            return $this;
        }

        foreach ($this->linksList as $base => $aliases) {
            if (in_array($alias, $aliases)) {
                foreach ($aliases as $i => $v) {
                    if ($v === $alias) {
                        unset($this->linksList[$base][$i]);
                    }
                }
            }
        }

        if (! array_key_exists($name, $this->linksList)) {
            $this->linksList[$name] = array();
        }

        if (! in_array($alias, $this->linksList[$name])) {
            $this->linksList[$name][] = $alias;
        }

        return $this;
    }

    public function isDefined($name)
    {
        foreach ($this->linksList as $base => $aliases) {
            if ($base === $name || in_array($name, $aliases)) {
                return true;
            }
        }

        return false;
    }

    public function isAlias($name)
    {
        foreach ($this->linksList as $aliases) {
            if (in_array($name, $aliases)) {
                return true;
            }
        }

        return false;
    }

    public function getBase($alias, $direct = false, $silent = false)
    {
        $base = $this->getParent($alias);

        $path = array();
        if (null !== $base && ! $direct) {
            $parent = $base;

            while (null !== $parent = $this->getParent($parent) and ! in_array($base, $path) and ! in_array($parent, $path)) {
                $path[] = $base = $parent;
            }

            if (null !== $parent and in_array($parent, $path)) {
                $msg = sprintf('Alias recursion detected at alias %s who\'s alias seems to be %s', $base, $parent);
                $msg .= sprintf(', traversed path ( %s )', implode(' -> ', $path));
                throw new LogicException($msg);
            }
        }

        if (null === $base && ! $silent) {
            $msg = sprintf('%s is unrecognized alias', $alias);
            if (! empty($path)) {
                $msg .= sprintf(', Unterminated dependency graph: ( %s )', implode(' -> ', $path));
                $msg .= sprintf(', as %s was not recognized', $path[count($path) - 1]);
            }
            throw new LogicException($msg);
        }

        return $base;
    }

    protected function getParent($alias)
    {
        foreach ($this->linksList as $name => $aliases) {
            if (in_array($alias, $aliases)) {
                return $name;
            }
        }

        return null;
    }

    public function hasAliases($name)
    {
        return ! empty($this->linksList[$name]);
    }

    public function hasAlias($name, $alias, $allowIndirect = true)
    {
        if (! $allowIndirect) {
            return ! empty($this->linksList[$name]) && in_array($alias, $this->linksList[$name]);
        }

        return in_array($alias, $this->getAliases($name, $allowIndirect));
    }

    public function getAliases($name, $allowIndirect = true)
    {
        if (! $allowIndirect) {
            return ! empty($this->linksList[$name]) ? $this->linksList[$name] : array();
        }

        if (empty($this->linksList[$name])) {
            return array();
        }

        $aliases = $this->linksList[$name];
        foreach ($aliases as $alias) {
            foreach ($this->getAliases($alias, true) as $candidate) {
                if (! in_array($candidate, $aliases)) {
                    $aliases[] = $candidate;
                }
            }
        }

        return $aliases;
    }
}