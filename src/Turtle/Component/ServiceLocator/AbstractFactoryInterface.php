<?php

namespace Turtle\Component\ServiceLocator;

use Turtle\Stl\ServiceLocatorInterface as LegacyServiceLocatorInterface;

interface AbstractFactoryInterface
{
    public function canCreateService(LegacyServiceLocatorInterface $serviceLocator, $name);
    public function createService(LegacyServiceLocatorInterface $serviceLocator, $name);
}