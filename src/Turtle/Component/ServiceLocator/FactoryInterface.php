<?php

namespace Turtle\Component\ServiceLocator;

use Turtle\Stl\ServiceLocatorInterface as LegacyServiceLocatorInterface;

interface FactoryInterface
{
    public function createService(LegacyServiceLocatorInterface $serviceLocator);
}