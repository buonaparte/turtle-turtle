<?php

namespace Turtle\Component\ServiceLocator;

use Turtle\Component\ServiceLocator\Exception\InvalidArgumentException;
use Turtle\Stl\ArrayUtils;

class Config implements ConfigInterface
{
    const SECTION_PARAMETERS = 'parameters';
    const SECTION_SERVICES = 'services';
    const SECTION_FACTORIES = 'factories';
    const SECTION_ABSTRACT_FACTORIES = 'abstract_factories';
    const SECTION_INITIALIZERS = 'initializers';
    const SECTION_ALIASES = 'aliases';
    const SECTION_SHARED = 'shared';

    protected $config = array();

    public function __construct($config = null)
    {
        if (null === $config) {
            return;
        }

        try {
            $this->config = ArrayUtils::toArray($config, true);
        } catch (\Turtle\Stl\Exception\InvalidArgumentException $e) {
            throw new InvalidArgumentException($e->getMessage());
        }
    }

    public function configureServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $map = array(
            'getParameters' => 'setParameter',
            'getServices' => 'setService',
            'getFactories' => 'setFactory'
        );

        foreach ($map as $accessor => $mutator) {
            foreach ($this->$accessor() as $key => $value) {
                $serviceLocator->$mutator($key, $value);
            }
        }

        $map = array(
            'getAbstractFactories' => 'addAbstractFactory',
            'getInitializers' => 'addInitializer'
        );

        foreach ($map as $accessor => $mutator) {
            foreach ($this->$accessor() as $value) {
                $serviceLocator->$mutator($value);
            }
        }

        foreach ($this->getAliases() as $name => $aliases) {
            foreach (ArrayUtils::toArray($aliases, false) as $alias) {
                $serviceLocator->setAlias($name, $alias);
            }
        }

        foreach ($this->getShared() as $service => $value) {
            $serviceLocator->setShared($service, $value);
        }
    }

    protected function getConfigSection($section)
    {
        if (! isset($this->config[$section])) {
            return array();
        }

        try {
            return ArrayUtils::toArray($this->config[$section], true);
        } catch (InvalidArgumentException $e) {
            return array();
        }
    }

    protected function getParameters()
    {
        return $this->getConfigSection(static::SECTION_PARAMETERS);
    }

    protected function getServices()
    {
        return $this->getConfigSection(static::SECTION_SERVICES);
    }

    protected function getFactories()
    {
        return $this->getConfigSection(static::SECTION_FACTORIES);
    }

    protected function getAbstractFactories()
    {
        return $this->getConfigSection(static::SECTION_ABSTRACT_FACTORIES);
    }

    protected function getInitializers()
    {
        return $this->getConfigSection(static::SECTION_INITIALIZERS);
    }

    protected function getAliases()
    {
        return $this->getConfigSection(static::SECTION_ALIASES);
    }

    protected function getShared()
    {
        return $this->getConfigSection(static::SECTION_SHARED);
    }
}