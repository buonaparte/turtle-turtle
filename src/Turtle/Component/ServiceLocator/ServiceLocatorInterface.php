<?php
/**
 * Created by JetBrains PhpStorm.
 * User: buonaparte
 * Date: 4/29/13
 * Time: 3:36 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Turtle\Component\ServiceLocator;

interface ServiceLocatorInterface extends \Turtle\Stl\ServiceLocatorInterface
{
    public function setParameter($name, $value);
    public function setService($name, $service);
    public function setFactory($name, $factory);
    public function setAlias($name, $alias);
    public function addAbstractFactory($abstractFactory);
    public function addInitializer($initializer);
    public function setShared($name, $value);
}