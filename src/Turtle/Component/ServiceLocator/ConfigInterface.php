<?php

namespace Turtle\Component\ServiceLocator;

interface ConfigInterface
{
    public function configureServiceLocator(ServiceLocatorInterface $serviceLocator);
}