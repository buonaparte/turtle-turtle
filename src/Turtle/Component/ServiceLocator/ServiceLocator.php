<?php
/**
 * Created by JetBrains PhpStorm.
 * User: buonaparte
 * Date: 4/29/13
 * Time: 3:32 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Turtle\Component\ServiceLocator;

use Turtle\Component\ServiceLocator\Exception\InvalidArgumentException;
use Turtle\Component\ServiceLocator\Exception\LogicException;
use Turtle\Component\ServiceLocator\Exception\RuntimeException;
use Turtle\Stl\ArrayUtils;

class ServiceLocator implements ServiceLocatorInterface
{
    const POINTER_PREFIX = '@';

    protected $parameters = array();
    protected $services = array();
    protected $factories = array();
    protected $abstractFactories = array();
    protected $initializers = array();
    protected $aliasGraph;

    protected $instances = array();
    protected $shared = array();
    protected $sharingPolicy = true;

    protected $frozen = false;
    protected $allowOverride = true;
    protected $silent = false;

    public function __construct($config = null)
    {
        $this->aliasGraph = new AliasGraph();

        if (null === $config) {
            return;
        }

        if (ArrayUtils::isIterable($config)) {
            $config = new Config($config);
        }

        if ($config instanceof ConfigInterface) {
            $config->configureServiceLocator($this);
        }
    }

    public function setSharingPolicy($flag)
    {
        $this->sharingPolicy = (boolean) $flag;
        return $this;
    }

    public function getSharingPolicy()
    {
        return $this->sharingPolicy;
    }

    public function freeze()
    {
        $this->frozen = true;
        return $this;
    }

    public function isFrozen()
    {
        return $this->frozen;
    }

    public function setAllowOverride($flag)
    {
        $this->allowOverride = (boolean) $flag;
        return $this;
    }

    public function getAllowOverride()
    {
        return $this->allowOverride;
    }

    public function setSilent($flag)
    {
        $this->silent = (boolean) $flag;
        return $this;
    }

    public function isSilent()
    {
        return $this->silent;
    }

    public function has($service)
    {
        if (array_key_exists($service, $this->parameters) || array_key_exists($service, $this->instances)) {
            return true;
        }

        if ($this->aliasGraph->isAlias($service)) {
            try {
                return $this->has($this->aliasGraph->getBase($service));
            } catch (\Exception $e) {
                if (! $this->isSilent())  {
                    throw $e;
                }
            }
        }

        if ($this->canCreate($service)) {
            return true;
        }

        return false;
    }

    public function get($service)
    {
        if (! $this->has($service)) {
            throw new InvalidArgumentException(sprintf(
                '%s service is not defined, maybe forgotten an alias?', $service));
        }

        if (array_key_exists($service, $this->parameters)) {
            return $this->parameters[$service];
        }

        if (array_key_exists($service, $this->instances)) {
            return $this->instances[$service];
        }

        try {
            if ($this->aliasGraph->isAlias($service)) {
                return $this->get($this->aliasGraph->getBase($service));
            }

            if ($this->canCreate($service)) {
                $instance = $this->create($service);

                if ($this->sharingPolicy || ! empty($this->shared[$service])) {
                    $this->instances[$service] = $instance;
                }

                return $instance;
            }
        } catch (RuntimeException $e) {
            if (! $this->silent) {
                throw $e;
            }
        }

        if (! $this->silent) {
            throw new RuntimeException(sprintf('%s could not be retrieved', $service));
        }

        return null;
    }

    public function create($name, $useAbstractFactories = true)
    {
        if (! $this->canCreate($name, $useAbstractFactories)) {
            throw new InvalidArgumentException(sprintf('%s service could not be created', $name));
        }

        $instance = null;

        if (array_key_exists($name, $this->factories)) {
            $instance = $this->createFromFactory($name);
        }

        if (null === $instance && array_key_exists($name, $this->services)) {
            $instance = $this->createFromKnownService($name);
        }

        if (null === $instance && $useAbstractFactories && $this->canCreateFromAbstractFactory($name)) {
            $instance = $this->createFromAbstractFactory($name);
        }

        if (null === $instance) {
            throw new RuntimeException(sprintf('Could not create %s service', $name));
        }

        $this->initialize($instance);
        return $instance;
    }

    protected function initialize($instance)
    {
        foreach ($this->initializers as $initializer) {
            call_user_func($initializer, $instance, $this);
        }
    }

    protected function createFromFactory($name)
    {
        if (! array_key_exists($name, $this->factories)) {
            throw new InvalidArgumentException(sprintf('%s Factory not found', $name));
        }

        $factory = $this->factories[$name];
        if (! is_callable($factory)) {
            if (is_string($factory) && ! is_callable($factory) && class_exists($factory, true)) {
                $factory = new $factory();
            }

            if ($factory instanceof FactoryInterface) {
                $factory = array($factory, 'createService');
            }

            if (! is_callable($factory)) {
                throw new InvalidArgumentException(
                    'A factory can only be callable | FactoryInterface class name | FactoryInterface instance');
            }

            $this->factories[$name] = $factory;
        }

        return call_user_func($this->factories[$name], $this);
    }

    protected function createFromAbstractFactory($name)
    {
        foreach ($this->abstractFactories as $abstractFactory) {
            /** @var $abstractFactory AbstractFactoryInterface */
            if ($abstractFactory->canCreateService($this, $name)) {
                return $abstractFactory->createService($this, $name);
            }
        }

        throw new RuntimeException(sprintf(
            'Could not create %s service, because %s',
            $name,
            empty($this->abstractFactories)
                ? 'there are no Abstract Factories defined'
                : 'any of Abstract Factories is able to create a service with this name'
        ));
    }

    protected function createFromKnownService($name)
    {
        if (! array_key_exists($name, $this->services)) {
            throw new InvalidArgumentException(sprintf('%s is not a known service', $name));
        }

        return $this->parseServiceDefinition($this->services[$name]);
    }

    private function parseServiceDefinition($service)
    {
        $params = array();

        if (is_array($service) && 1 === count($service)) {
            $serviceName = array_keys($service);
            $serviceName = $serviceName[0];
            $params = array_values((array) $service[$serviceName]);
        } else {
            $serviceName = $service;
        }

        if ($this->isServicePointer($serviceName)) {
            try {
                $serviceName = (string) $this->resolveServicePointer($serviceName);
            } catch (InvalidArgumentException $e) {
                throw new RuntimeException(sprintf(
                    'Could not locate service %s, which must result in a service name', $serviceName));
            }
        }

        if (is_string($serviceName) && class_exists($serviceName, true)) {
            foreach ($params as $i => $parameterName) {
                if (! is_string($parameterName)) {
                    continue;
                }

                if ($this->isServicePointer($parameterName)) {
                    try {
                        $params[$i] = $this->resolveServicePointer($parameterName);
                    } catch (\Exception $e) {
                        throw new RuntimeException(sprintf(
                            'Unlocated service %s as %s constructor parameter',
                            $this->extractServiceNameFromPointer($parameterName), $serviceName));
                    }
                // Notice: the "parameterName" could be escaped by {ServiceLocator::isServicePointer()}
                } else {
                    $params[$i] = $parameterName;
                }
            }

            $reflection = new \ReflectionClass($serviceName);
            return $reflection->newInstanceArgs($params);
        }

        throw new InvalidArgumentException(sprintf('Not a valid Service definition provided for %s', $serviceName));
    }

    private function extractServiceNameFromPointer($serviceDef)
    {
        if (is_string($serviceDef) && static::POINTER_PREFIX === substr($serviceDef, 0, strlen(static::POINTER_PREFIX))) {
            return substr($serviceDef, strlen(static::POINTER_PREFIX));
        }

        return $serviceDef;
    }

    private function resolveServicePointer($name)
    {
        if (! $this->isServicePointer($name)) {
            throw new InvalidArgumentException(sprintf('%s is not a valid service pointer definition', $name));
        }

        $service = $this->get($this->extractServiceNameFromPointer($name));
        if (is_string($service) && $this->isServicePointer($service)) {
            do {
                $service = $this->resolveServicePointer($service);
            } while (is_string($service) && $this->isServicePointer($service));
        }

        return $service;
    }

    private function isServicePointer(&$serviceDef)
    {
        if (is_string($serviceDef) && '\\' . static::POINTER_PREFIX === substr($serviceDef, 0, strlen(static::POINTER_PREFIX) + 1)) {
            $serviceDef = substr($serviceDef, 1);
            return false;
        }

        if (is_string($serviceDef) && static::POINTER_PREFIX === substr($serviceDef, 0, strlen(static::POINTER_PREFIX))) {
            return true;
        }

        return false;
    }

    public function canCreate($name, $useAbstractFactories = true)
    {
        if (isset($this->services[$name]) || $this->canCreateFromFactory($name)) {
            return true;
        }

        if ($useAbstractFactories && $this->canCreateFromAbstractFactory($name)) {
            return true;
        }

        return false;
    }

    public function canCreateFromFactory($name)
    {
        return array_key_exists($name, $this->factories);
    }

    public function canCreateFromAbstractFactory($name)
    {
        foreach ($this->abstractFactories as &$abstractFactory) {
            if (! $abstractFactory instanceof AbstractFactoryInterface) {
                if (is_string($abstractFactory) && class_exists($abstractFactory, true)) {
                    $abstractFactory = new $abstractFactory();
                }

                if (! $abstractFactory instanceof AbstractFactoryInterface) {
                    throw new InvalidArgumentException(
                        'An abstract factory can only be AbstractFactoryInterface class name
                            | AbstractFactoryInterface instance');
                }
            }

            /** @var $abstractFactory AbstractFactoryInterface */
            if ($abstractFactory->canCreateService($this, $name)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Register a parameter.
     *
     * @param $name string parameter Id
     * @param $value mixed parameter Value
     * @throws Exception\LogicException if Service Locator is frozen
     * @return $this ServiceLocator current instance
     */
    public function setParameter($name, $value)
    {
        if ($this->isFrozen()) {
            throw new LogicException(sprintf('Could not add parameter %s, because the Service Locator is frozen', $name));
        }

        $this->parameters[$name] = $value;
        return $this;
    }

    /**
     * Define a class name for a service.
     * In case there are only hard - dependencies for the service,
     * that can be pulled from current ServiceLocator instance,
     * it can be easier to define it as a Service Definition instead of using a factory.
     *
     * @param $name string service Id
     * @param $service string | array service class name or an array as service definition,
     * if the class constructor expects some mandatory arguments to pull from the current ServiceLocator
     * @throws Exception\LogicException if Service Locator is frozen
     * @return $this ServiceLocator current instance
     */
    public function setService($name, $service)
    {
        if ($this->isFrozen()) {
            throw new LogicException(sprintf('Could not add service %s, because the Service Locator is frozen', $name));
        }

        $this->services[$name] = $service;
        return $this;
    }

    /**
     * Define a factory for a service.
     * Attempt to add multiple factories for the same service Id will result in override.
     *
     * @param $name string service Id
     * @param $factory callable | string | FactoryInterface the Factory
     * @return $this ServiceLocator current instance
     * @throws Exception\LogicException if Service Locator is frozen
     * @throws Exception\InvalidArgumentException if provided factory is not valid
     */
    public function setFactory($name, $factory)
    {
        if ($this->isFrozen()) {
            throw new LogicException(sprintf(
                'Could not add factory for service %s, because the Service Locator is frozen', $name));
        }

        $this->factories[$name] = $factory;
        return $this;
    }

    /**
     * Register an abstract factory to the ServiceLocator.
     *
     * @param $abstractFactory string | AbstractFactoryInterface the Abstract Factory
     * @return $this ServiceLocator current instance
     * @throws Exception\InvalidArgumentException if the provided factory is not valid
     * @throws Exception\LogicException if Service Locator is frozen
     */
    public function addAbstractFactory($abstractFactory)
    {
        if ($this->isFrozen()) {
            throw new LogicException('Could not add Abstract Factory, because the Service Locator is frozen');
        }

        $this->abstractFactories[] = $abstractFactory;
        return $this;
    }

    /**
     * Register an initializer to the ServiceLocator.
     *
     * @param $initializer callable | string | IntializerInterface the Initializer
     * @return $this ServiceLocator current instance
     * @throws Exception\InvalidArgumentException if the provided initializer is not valid
     * @throws Exception\LogicException if Service Locator is frozen
     */
    public function addInitializer($initializer)
    {
        if ($this->isFrozen()) {
            throw new LogicException('Could not add initializer, because the Service Locator is frozen');
        }

        if (is_string($initializer) && ! is_callable($initializer) && class_exists($initializer, true)) {
            $initializer = new $initializer();
        }

        if ($initializer instanceof IntializerInterface) {
            $initializer = array($initializer, 'initializeService');
        }

        if (! is_callable($initializer)) {
            throw new InvalidArgumentException(
                'A initializer must be callable | class name | InitializerInterface instance');
        }

        $this->initializers[] = $initializer;
        return $this;
    }

    /**
     * Register an alias, notice, it is also possible to add an alias to alias,
     * which will result in the same original service pointer.
     * An attempt to add the same alias to multiple services, will result in
     * overriding the original link.
     *
     * @param $name string baseName
     * @param $alias string service alias
     * @throws Exception\LogicException if Service Locator is frozen
     * @return $this ServiceLocator current instance
     */
    public function setAlias($name, $alias)
    {
        if ($this->isFrozen()) {
            throw new LogicException(sprintf(
                'Could not alias %s for %s, because the Service Locator is frozen', $alias, $name));
        }

        $this->aliasGraph->addAlias($name, $alias);
        return $this;
    }

    /**
     * Mark a service by Id to be shared in the ServiceLocator, no matter the sharing Policy of
     * the ServiceLocator at the moment of retrieval.
     *
     * @param $name string service Id
     * @param $value boolean True if the service will be shared, False otherwise
     * @throws Exception\LogicException if Service Locator is frozen
     * @return $this ServiceLocator current instance
     */
    public function setShared($name, $value)
    {
        if ($this->isFrozen()) {
            throw new LogicException(sprintf(
                'Could not mark %s as %s, because the Service Locator is frozen',
                $name,
                $value ? 'shared' : 'not shared'
            ));
        }

        $this->shared[$name] = (boolean) $value;
        return $this;
    }
}