<?php

namespace Turtle\Component\ServiceLocator;

use Turtle\Stl\ServiceLocatorInterface as LegacyServiceLocatorInterface;

interface IntializerInterface
{
    public function initializeService($instance, LegacyServiceLocatorInterface $serviceLocator);
}