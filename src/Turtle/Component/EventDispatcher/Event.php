<?php

namespace Turtle\Component\EventDispatcher;

use Turtle\Component\EventDispatcher\Exception\InvalidArgumentException;

class Event implements
    EventInterface,
    \ArrayAccess
{
    protected $name;
    protected $subject;
    protected $value = null;
    protected $params = array();
    protected $processed = false;

    public function __construct($name, $subject = null, $params = null)
    {
        $this->name = (string) $name;

        $this->setSubject($subject)
             ->setParameters($params);
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setProcessed($flag = true)
    {
        $this->processed = (boolean) $flag;
        return $this;
    }

    public function isProcessed()
    {
        return $this->processed;
    }

    public function setReturnValue($value)
    {
        $this->value = $value;
    }

    public function hasReturnValue()
    {
        return null !== $this->value;
    }

    public function getReturnValue()
    {
        return $this->value;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setParameters($parameters = null)
    {
        if (null === $parameters) {
            $this->params = array();
            return $this;
        }

        if (! is_array($parameters) && ! $parameters instanceof \Traversable) {
            throw new InvalidArgumentException(sprintf('Event parameters can only be %s', 'array | Traversable'));
        }

        foreach ($parameters as $name => $value) {
            $this->params[$name] = $value;
        }

        return $this;
    }

    public function hasParameter($name)
    {
        return array_key_exists($name, $this->params);
    }

    public function getParameter($name, $default = null)
    {
        return $this->hasParameter($name)
            ? $this->params[$name]
            : $default;
    }

    public function setParameter($name, $value, $override = true)
    {
        if ($this->hasParameter($name) && ! $override) {
            return $this;
        }

        $this->params[$name] = $value;
        return $this;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset)
    {
        return $this->hasParameter($offset);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        return $this->getParameter($offset);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->setParameter($offset, $value, true);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->params[$offset]);
    }
}