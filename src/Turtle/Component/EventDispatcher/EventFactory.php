<?php

namespace Turtle\Component\EventDispatcher;

class EventFactory implements EventFactoryInterface
{
    public function createEvent($eventName, $subject = null, $args = null)
    {
        if ($eventName instanceof EventInterface) {
            return $eventName;
        }

        return new Event($eventName, $subject, $args);
    }
}