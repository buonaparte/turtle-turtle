<?php
/**
 * Created by JetBrains PhpStorm.
 * User: buonaparte
 * Date: 4/29/13
 * Time: 1:22 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Turtle\Component\EventDispatcher\Exception;

class LogicException extends \LogicException implements ExceptionInterface
{
}