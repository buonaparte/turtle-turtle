<?php
/**
 * Created by JetBrains PhpStorm.
 * User: buonaparte
 * Date: 4/29/13
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Turtle\Component\EventDispatcher;

interface EventInterface
{
    public function getName();
    public function setSubject($subject);
    public function isProcessed();
    public function setReturnValue($value);
}