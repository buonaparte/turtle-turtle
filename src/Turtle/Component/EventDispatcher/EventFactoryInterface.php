<?php

namespace Turtle\Component\EventDispatcher;

interface EventFactoryInterface
{
    public function createEvent($eventName, $subject = null, $args = null);
}