<?php

namespace Turtle\Component\EventDispatcher;

use Turtle\Component\EventDispatcher\Exception\InvalidArgumentException;
use Turtle\Component\EventDispatcher\Exception\LogicException;
use Turtle\Stl\ArrayUtils;
use Turtle\Stl\EventDispatcher\DispatcherInterface;
use Turtle\Stl\EventDispatcher\SubscriberInterface;

class Dispatcher implements DispatcherInterface
{
    /**
     * A Dictionary, where Keys are Event Names (wildcards are accepted as well),
     * Values are represented by an Array of two values - array(Listener, Priority)
     * @var array
     */
    protected $listeners = array();

    /**
     * A list of Subscribers, each Item is represented by an Array of two values -
     * array(Subscriber, Listeners) where Listeners is a structure like that of internal
     * listeners stored by the Dispatcher
     * @var array
     */
    protected $subscribers = array();

    /**
     * The Factory used by this Dispatcher to create event instances, when notifying named events
     * @var callable
     */
    protected $eventFactory;

    /**
     * Connects a listener to an event
     *
     * Event parameter can also be a list which behavior will connect
     * the listener to a list of given events
     *
     * @param array|\Traversable|string $event Event(s) to connect to
     * @param callable $listener The listener
     * @param int $priority The listener priority in the list of this Dispatcher aggregated listeners
     * for the same event
     * @return Dispatcher Returns self for a fluent interface
     * @throws Exception\InvalidArgumentException if the provided listener is not a valid callable
     */
    public function connect($event, $listener, $priority = 0)
    {
        if (! is_callable($listener)) {
            throw new InvalidArgumentException('A listener can be only a valid callable');
        }

        if (! ArrayUtils::isIterable($event)) {
            $event = array($event);
        }

        foreach ($event as $eventName) {
            if (! array_key_exists($eventName, $this->listeners)) {
                $this->listeners[$eventName] = array();
            }

            $this->listeners[$eventName][] = array($listener, $priority);
        }

        return $this;
    }

    /**
     * Returns an Array of listeners connected to an event or a list of events passed as the first parameter.
     * Notice, if the first parameter is a list of events, an Array of the following structure will be returned:
     *
     * array(
     *  FirstEventName => its listeners,
     *  SecondEventName => its listeners,
     *  ...
     * )
     *
     * @param string|array|\Traversable $event Event(s) to search for
     * @param bool $sort Sort the listeners
     * @param bool $preservePriorities If set to true will return an Array of array with 2 elements -
     * Listener, priority
     * @return array
     */
    public function getListeners($event, $sort = true, $preservePriorities = false)
    {
        $events = array_flip($this->getEventNames($event));

        $listeners = array_intersect_key($this->listeners, $events);
        if (empty($listeners)) {
            return array();
        }

        if ($sort) {
            foreach ($listeners as &$eventListeners) {
                usort($eventListeners, array($this, 'listenersSortCallback'));
            }
            unset($eventListeners);
        }

        if (! $preservePriorities) {
            foreach ($listeners as &$eventListeners) {
                foreach ($eventListeners as &$listener) {
                    $listener = $listener[0];
                }
            }
        }

        if (ArrayUtils::isIterable($event)) {
            return $listeners;
        }

        foreach ($listeners as $eventListeners) {
            return $eventListeners;
        }

        return array();
    }

    /**
     * Check if a listener is connected to any of Event(s)
     *
     * @param string|array|\Traversable $event Event(s) to search for
     * @param callable $listener The lister to test
     * @return bool
     */
    public function isConnected($event, $listener)
    {
        foreach ($this->getListeners($event) as $candidate) {
            if ($candidate === $listener) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if the subscriber is aggregated by the Dispatcher
     *
     * @param SubscriberInterface $subscriber Subscriber to test
     * @return bool
     */
    public function isSubscribed(SubscriberInterface $subscriber)
    {
        foreach ($this->subscribers as $ctx) {
            if ($ctx[0] === $subscriber) {
                return true;
            }
        }

        return false;
    }

    /**
     * Sort function to order a list of listeners by their priorities
     *
     * @param callable $handler1 First listener for the test function
     * @param callable $handler2 Second listener for the test function
     * @return bool
     */
    protected function listenersSortCallback($handler1, $handler2)
    {
        foreach (array('handler1', 'handler2') as $target) {
            if (empty(${$target}[1])) {
                ${$target}[1] = 0;
            }
        }

        return (int) $handler1[1] <= (int) $handler2[1];
    }

    /**
     * Gets the EventName, if an EventInterface is passed
     *
     * @param mixed $event
     * @return string
     */
    protected function getEventName($event)
    {
        if ($event instanceof EventInterface) {
            return $event->getName();
        }

        return  (string) $event;
    }

    /**
     * Gets an Array of EventNames, collected using Dispatcher::getEventName()
     *
     * @param mixed $events
     * @return array
     */
    protected function getEventNames($events)
    {
        if (! ArrayUtils::isIterable($events)) {
            $events = array($events);
        }

        return array_map(array($this, 'getEventName'), $events);
    }

    /**
     * Return the Event Factory used by this Dispatcher,
     * will lazy - load the default one (EventFactory) if non provided
     *
     * @return callable
     */
    public function getEventFactory()
    {
        if (null === $this->eventFactory) {
            $this->setEventFactory(new EventFactory());
        }

        return $this->eventFactory;
    }

    /**
     * Specifies the Event Factory to use in this Dispatcher
     *
     * @param callable|string|EventFactoryInterface $eventFactory
     * @return Dispatcher for fluent interface
     * @throws Exception\InvalidArgumentException If the provided factory is not valid
     */
    public function setEventFactory($eventFactory)
    {
        if (is_string($eventFactory) && ! is_callable($eventFactory) && class_exists($eventFactory)) {
            $eventFactory = new $eventFactory;
        }

        if ($eventFactory instanceof EventFactoryInterface) {
            $eventFactory = array($eventFactory, 'createEvent');
        }

        if (! is_callable($eventFactory)) {
            throw new InvalidArgumentException(
                'Event Factory can only be a class name | EventFactoryInterface instance | callable');
        }

        $this->eventFactory = $eventFactory;
        return $this;
    }

    /**
     * Checks if there are any listeners connected to the specified events
     *
     * @param string|array|\Traversable $event Event(s) to search for
     * @return bool
     */
    public function hasListeners($event)
    {
        $events = $this->getEventNames($event);
        foreach(array_intersect_key($events, $this->listeners) as $listeners) {
            if (! empty($listeners)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Disconnects a listener or all listener for a given single Event
     *
     * @param string $eventName Event from which the listener(s) will be disconnected
     * @param null $listener Listener or null for all listeners
     * @return bool
     */
    public function disconnect($eventName, $listener = null)
    {
        if (! array_key_exists($eventName, $this->listeners)) {
            return false;
        }

        if (null === $listener) {
            unset($this->listeners[$eventName]);
            return true;
        }

        $found = false;
        foreach ($this->listeners[$eventName] as $i => $ctx) {
            if ($ctx[0] === $listener) {
                unset($this->listeners[$eventName][$i]);
                $found = true;
            }
        }

        return $found;
    }

    /**
     * Subscribes the instance to the events aggregated by this Dispatcher.
     * The following results on SubscriberInterface::getSubscribedEvents() call are understandable by this Dispatcher:
     *
     *  * Array|\Traversable of EventName => valid callable
     *  * Array|\Traversable of EventName => Array of 2 elements - valid callable and priority
     *  * Array|\Traversable of EventName => string - a public visible method on the passed Subscriber instance
     *
     * Any other combination of the above will be accepted as well.
     *
     * @param SubscriberInterface $subscriber
     * @return bool True if the Subscriber will listen to at list one event from this Dispatcher
     * @throws Exception\LogicException If the subscriber returned a non-traversable result on
     * SubscriberInterface::getSubscribedEvents call
     */
    public function subscribe(SubscriberInterface $subscriber)
    {
        $events = $subscriber->getSubscribedEvents();
        if (! is_array($events) && ! $events instanceof \Traversable) {
            throw new LogicException(sprintf(
                '%s must return array | Traversable of subscribed events',
                'SubscriberInterface::getSubscribedEvents()'));
        }

        $indexes = array();
        foreach ($events as $name => $handlers) {
            foreach ($this->normalizeSubscriberListeners($subscriber, $handlers) as $handler) {
                list($listener, $priority) = $handler;
                $this->connect($name, $listener, $priority);
                $indexes[$name][] = $listener;
            }
        }

        if (! empty($indexes)) {
            $this->subscribers[] = array($subscriber, $indexes);
            return true;
        }

        return false;
    }

    /**
     * Dispatcher::subscribe() method helper
     *
     * @param SubscriberInterface $subscriber
     * @param $handlers
     * @return array
     */
    protected function normalizeSubscriberListeners(SubscriberInterface $subscriber, $handlers)
    {
        if (! $this->normalizeSubscriberListener($subscriber, $handlers)) {
            foreach ($handlers as &$handler) {
                if (! $this->normalizeSubscriberListener($subscriber, $handler)) {
                    $handler = array($handler, 0);
                }
            }

            return $handlers;
        }

        return array($handlers);
    }

    /**
     * Dispatcher::subscribe() method helper
     *
     * @param SubscriberInterface $subscriber
     * @param $handler
     * @return bool
     */
    protected function normalizeSubscriberListener(SubscriberInterface $subscriber, &$handler)
    {
        if (is_string($handler)
            && method_exists($subscriber, $handler)) {

            $handler = array($subscriber, $handler);
        }

        if (is_callable($handler)) {
            $handler = array($handler, 0);
        } elseif (is_array($handler)
            && 2 == count($handler = array_values($handler))
            && ! is_callable($handler)) {

            $handler = array($handler[0], (int) $handler[1]);
        } else {
            return false;
        }

        return true;
    }

    /**
     * UnSubscribe a single or all Subscriber aggregated by this Dispatcher.
     * The dispatcher will also disconnect all the listeners specified by the Subscriber(s)
     *
     * @param SubscriberInterface|null $subscriber a single subscriber or null for all
     * @return bool If the Dispatcher was aggregating the passed subscriber
     * or at least one subscriber
     */
    public function unSubscribe(SubscriberInterface $subscriber = null)
    {
        if (null === $subscriber) {
            $res = ! empty($this->subscribers);
            $this->subscribers = array();

            return $res;
        }

        $indexes = array();
        foreach ($this->subscribers as $i => $context) {
            if ($context[0] === $subscriber) {
                $indexes = $context[1];
                unset($this->subscribers[$i]);

                break;
            }
        }

        if (empty($indexes)) {
            return false;
        }

        $success = true;
        foreach ($indexes as $name => $listeners) {
            foreach ($listeners as $listener) {
                if (! $this->disconnect($name, $listener)) {
                    $success = false;
                }
            }
        }

        return $success;
    }

    /**
     * Returns an EventInterface instance, if a string is passed, the Dispatcher will invoke the
     * EventFactory and try to create an instance based on the provided parameters
     *
     * @param mixed $event
     * @param object|null $subject The Event subject
     * @param array|\Traversable|null $args The Event parameters
     * @return EventInterface
     * @throws Exception\LogicException If a valid EventInstance could not be created by the Factory
     */
    protected function getEvent($event, $subject = null, $args = null)
    {
        if (! $event instanceof EventInterface) {
            $factory = $this->getEventFactory();
            $event = call_user_func($factory, $event, $subject, $args);

            if (! $event instanceof EventInterface) {
                throw new LogicException('Supplied Event Factory did not return a valid EventInterface instance');
            }
        }

        return $event;
    }

    /**
     * Notifies all connected listeners about an event.
     * The following combination of parameters acceptance must be implemented:
     *
     *  * Event instance as the only parameter
     *  * EventName and possible a subject and some arguments that will be used in cases
     *      when the process of creating an Event instance will be forwarded to the Dispatcher itself
     *
     * @param string|object $event The EventName or an Event instance
     * @param object|null $subject The subject of the event
     * @param array|\Traversable|null $args Parameters for the future Event
     * @return object The notified Event object
     */
    public function notify($event, $subject = null, $args = null)
    {
        $event = $this->getEvent($event, $subject, $args);
        foreach ($this->getListeners($event) as $listener) {
            call_user_func($listener, $event);
        }

        return $event;
    }

    public function notifyUntil($event, $subject = null, $args = null, $processedTest = null)
    {
        if (null === $processedTest) {
            $processedTest = function (EventInterface $event) {
                return $event->isProcessed();
            };
        }

        if (! is_callable($processedTest)) {
            throw new InvalidArgumentException('Event "processed - test" must be a valid callable');
        }

        $event = $this->getEvent($event, $subject, $args);

        foreach ($this->getListeners($event) as $listener) {
            call_user_func($listener, $event);
            if (call_user_func($processedTest, $event)) {
                break;
            }
        }

        return $event;
    }

    public function filter($event, $subject = null, $args = null)
    {
        $event = $this->getEvent($event);

        $value = null;
        foreach ($this->getListeners($event) as $listener) {
            $value = call_user_func($listener, $event);
        }

        $event->setReturnValue($value);

        return $this;
    }
}