<?php
/**
 * Created by JetBrains PhpStorm.
 * User: buonaparte
 * Date: 5/7/13
 * Time: 4:20 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Turtle\Stl;

interface ResponseInterface
{
    public function send();
}