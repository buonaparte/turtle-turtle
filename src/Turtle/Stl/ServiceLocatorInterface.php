<?php
/**
 * Created by JetBrains PhpStorm.
 * User: buonaparte
 * Date: 4/29/13
 * Time: 3:28 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Turtle\Stl;

interface ServiceLocatorInterface
{
    public function has($service);
    public function get($service);
}