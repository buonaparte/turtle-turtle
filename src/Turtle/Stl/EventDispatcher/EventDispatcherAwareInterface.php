<?php

namespace Turtle\Stl\EventDispatcher;

interface EventDispatcherAwareInterface
{
    /**
     * Injects an instance of EventDispatcher Interface
     *
     * @param DispatcherInterface $eventDispatcher
     * @return mixed
     */
    public function setEventDispatcher(DispatcherInterface $eventDispatcher);

    /**
     * Get EventDispatcher Instance attached, if non was injected -
     * the method call will Lazy - Load the default EventDispatcher.
     *
     * @return DispatcherInterface
     */
    public function getEventDispatcher();
}