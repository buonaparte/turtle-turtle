<?php

namespace Turtle\Stl\EventDispatcher;

interface DispatcherInterface
{
    public function connect($event, $listener, $priority = 0);
    public function disconnect($eventName, $listener = null);

    public function subscribe(SubscriberInterface $subscriber);
    public function unSubscribe(SubscriberInterface $subscriber = null);

    /**
     * Notifies all connected listeners about an event.
     * The following combination of parameters acceptance must be implemented:
     *
     *  * Event instance as the only parameter
     *  * EventName and possible a subject and some arguments that will be used in cases
     *      when the process of creating an Event instance will be forwarded to the Dispatcher itself
     *
     * @param string|object $event The EventName or an Event instance
     * @param object|null $subject The subject of the event
     * @param array|\Traversable|null $args Parameters for the future Event
     * @return object The notified Event object
     */
    public function notify($event, $subject = null, $args = null);

    /**
     * Notifies all connected listeners in order, about an event, until the processTest callback is evaluated to True.
     * The single difference between DispatcherInterface::notify() is that this will be used for short circuiting.
     * The Dispatcher implementation will be responsible for checking if the propagation must be stopped, passing
     * the Event object as a single argument for the processTest.
     * The following combination of parameters acceptance must be implemented:
     *
     *  * Event instance as the only parameter, in which case the Dispatcher implementation must provide a default
     *      processedTest test callable
     *  * EventName and possible a subject and some arguments that will be used in cases
     *      when the process of creating an Event instance will be forwarded to the Dispatcher itself
     *
     * @param string|object $event The EventName or an Event instance
     * @param object|null $subject The subject of the event
     * @param array|\Traversable|null $args Parameters for the future event
     * @param callable|null $processedTest The test callback, used in short circuits
     * @return object The notified Event object
     */
    public function notifyUntil($event, $subject = null, $args = null, $processedTest = null);


    public function filter($event, $target = null, $args = null);
}