<?php

namespace Turtle\Stl\Hydrator;

class ClassMethods implements HydratorInterface
{
    const OPTIONS_KEY_ACCESSORS = 'accessors';
    const OPTIONS_KEY_MUTATORS = 'mutators';
    const OPTIONS_KEY_ALLOWOVERWRITE = 'allowOverwrite';

    protected $options = array(
        self::OPTIONS_KEY_ALLOWOVERWRITE => true
    );

    public function __construct(array $options = array())
    {
        $this->options = $this->getDefaultOptions(array_merge_recursive($this->options, $options));
    }

    public function hydrate(array $data, $object)
    {
        $methods = get_class_methods(get_class($object));

        $defaultClosure = function ($value) {return $value;};
        $mutators = $this->options[self::OPTIONS_KEY_MUTATORS];
        foreach ($mutators as &$mutator) {
            if (null === $mutator) {
                $mutator = $defaultClosure;
            }
        }
        unset($mutator);

        foreach ($data as $property => $value) {
            foreach ($mutators as $prefix => $callback) {
                if (method_exists($object, $mutator = $prefix . ucfirst($property))) {
                    $object->{$mutator}(call_user_func($callback, $value));
                }
            }
        }

        return $object;
    }

    public function extract($object, array $data = array())
    {
        $methods = get_class_methods($object);

        $defaultClosure = function ($value) {return $value;};
        $accessors = $this->options[self::OPTIONS_KEY_ACCESSORS];
        foreach ($accessors as &$accessor) {
            if (null === $accessor) {
                $accessor = $defaultClosure;
            }
        }
        unset($accessor);

        foreach ($methods as $accessor) {
            if (! method_exists($object, $accessor)) {
                continue;
            }

            foreach ($accessors as $prefix => $normalizer) {
                if ($property = substr($accessor, 0, strlen($prefix))) {
                    $property[0] = strtolower($property[0]);

                    if (! $this->options[self::OPTIONS_KEY_ALLOWOVERWRITE] && array_key_exists($property, $data)) {
                        continue;
                    }

                    $data[$property] = call_user_func($normalizer, $object->{$accessor}());
                }
            }
        }

        return $data;
    }

    protected function getDefaultOptions(array $replacements = array())
    {
        return array_merge_recursive(array(
            self::OPTIONS_KEY_ACCESSORS => array(
                'is' => function ($value) {
                    return (boolean) $value;
                },
                'get' => null,
                'has' => function ($value) {
                    return (boolean) $value;
                }
            ),
            self::OPTIONS_KEY_MUTATORS => array(
                'set' => null
            )
        ), $replacements);
    }
}