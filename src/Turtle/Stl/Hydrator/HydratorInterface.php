<?php
/**
 * Created by JetBrains PhpStorm.
 * User: buonaparte
 * Date: 4/29/13
 * Time: 11:55 AM
 * To change this template use File | Settings | File Templates.
 */

namespace Turtle\Stl\Hydrator;

interface HydratorInterface
{
    public function hydrate(array $data, $object);
    public function extract($object, array $data = array());
}