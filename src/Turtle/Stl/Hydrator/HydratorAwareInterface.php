<?php

namespace Turtle\Stl\Hydrator;

interface HydratorAwareInterface
{
    /**
     * @param HydratorInterface $hydrator
     * @return mixed
     */
    public function setHydrator(HydratorInterface $hydrator);

    /**
     * Retrieves the current Hydrator in use, or attempts to Lazy - load one if none was specified
     *
     * @return HydratorInterface
     */
    public function getHydrator();
}