<?php
/**
 * Created by JetBrains PhpStorm.
 * User: buonaparte
 * Date: 4/29/13
 * Time: 4:02 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Turtle\Stl\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}