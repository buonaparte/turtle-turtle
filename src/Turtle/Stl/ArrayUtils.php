<?php
/**
 * Created by JetBrains PhpStorm.
 * User: buonaparte
 * Date: 4/29/13
 * Time: 3:57 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Turtle\Stl;

use Turtle\Stl\Exception\InvalidArgumentException;

abstract class ArrayUtils
{
    public static function isIterable($value)
    {
        return is_array($value) || $value instanceof \Traversable;
    }

    public static function isAssoc($value, $silent = true)
    {
        try {
            $value = ArrayUtils::toArray($value);
        } catch (InvalidArgumentException $e) {
            if ($silent) {
                return false;
            }

            throw $e;
        }

        foreach (array_keys($value) as $key) {
            if (! is_numeric($key)) {
                return true;
            }
        }

        return true;
    }

    public static function isList($value, $successive = true, $silent = true)
    {
        $keys = array_keys($value = static::toArray($value));
        $prev = reset($keys);
        while ($key = next($keys)) {
            if (! is_numeric($prev) || ! is_numeric($key) || $successive and 1 != abs($key - $prev)) {
                return false;
            }

            $prev = $key;
        }

        return 1 == count($keys) ? is_numeric($prev) : true;
    }

    public static function toArray($value, $strict = true)
    {
        if (! static::isIterable($value)) {
            if (! $strict) {
                return (array) $value;
            }

            throw new InvalidArgumentException('Array | Traversable expected');
        }

        if (is_array($value)) {
            return $value;
        }

        $res = array();
        foreach ($value as $k => $v) {
            $res[$k] = $v;
        }

        return $res;
    }

    public static function merge($data)
    {
        $args = func_get_args();

        return call_user_func_array(
            'array_merge',
            array_map('Turtle\\Stl\\ArrayUtils::toArray', $args)
        );
    }

    public static function mergeRecursive($data)
    {
        $args = func_get_args();

        return call_user_func_array(
            'array_merge_recursive',
            array_map('Turtle\\Stl\\ArrayUtils::toArray', $args)
        );
    }

    public static function mergeRecursiveDistinct($data)
    {
        $merged = self::toArray($data);

        $args = func_get_args();
        array_shift($args);

        foreach (array_map('Turtle\\Stl\\ArrayUtils::toArray', $args) as $arg) {
            foreach ($arg as $k => &$v) {
                if (self::isIterable($v) && array_key_exists($k, $merged) && self::isIterable($merged[$k])) {
                    $merged[$k] = self::mergeRecursiveDistinct($merged[$k], $v);
                } else {
                    $merged[$k] = $v;
                }
            }
        }

        return $merged;
    }
}